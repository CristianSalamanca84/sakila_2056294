<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Añadir Categoria</title>
</head>
<body>
    <!--Si la variable de sesion "mensaje" existe, la muyestra-->
    @if(session("mensaje"))
        <p class="alert-success"> {{    session("mensaje") }}  </p>
    @else
        
    @endif
    <form method="POST" action='{{ url("categorias/update/$categoria->category_id") }}' class="form-horizontal">
        @csrf
        <fieldset>
        
        <!-- Form Name -->
        <legend>Editar Categoria</legend>
        
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="textinput">Nombre de Categoria</label>  
          <div class="col-md-4">
          <input id="textinput" name="categoria" value="{{ $categoria->name }}" type="text" placeholder="" class="form-control input-md">
          <strong class="text-danger"> {{  $errors->first("categoria") }} </strong>
          
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="singlebutton">Single Button</label>
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Editar</button>
          </div>
        </div>
        
        </fieldset>
        </form>        
</body>
</html>