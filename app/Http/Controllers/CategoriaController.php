<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    public function index(){
        $categoria = Categoria::paginate(5);
        return view("categoria.index")->with("categoria" , $categoria);  
    }
    //mostrar el formulario de crear categoria 
    public function create(){
        //echo "Formlario de Categoria";
        return view('categoria.new');
    }
    //llegar los datos desde el formulario
    //guardar la categoria en BD
    public function store(Request $r){
        $reglas =[ 
            "categoria" =>  ["required", "alpha"]
        ];

        $mensajes = [ 
            "required" => "Campo Obligatorio",
            "alpha" => "Solo letras"
        ];

        //2. Crear el objeto validador
        $validador = Validator::make($r->all() , $reglas, $mensajes);

        //3. Validar: metodo fails
        // retorna true(v) si la validacion falla
        // retorna falso en caso de que los datos sean correctos
        if ($validador->fails()) {
            //Codigo para cuando falla
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo cuando la validacion es correcta 
        }
        //$_POST arreglo PHP
        //Almacena la informacion que viede desde formularios
        //crear nueva categoria
        $categoria = new Categoria();
        //asignar el nombre, desde formulario
        $categoria->name = $r->input("categoria");
        //guardar nueva categoria
        $categoria->save();
        //redireccio con datos de sesion
        return redirect('categorias/create')->with("mensaje" , "Categoria Guardada");
    }

    public function edit($category_id){
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //mostrar la vista de actualizar categotia
        //levando dentro la categoria
        return view("categoria.edit")->with("categoria", $categoria);
    }

    public function update($category_id){ 

        $categoria = Categoria::find($category_id);
        //editar sus atributos
        $categoria->name = $_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //retornar al formulario
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria Editada");

    }
}
